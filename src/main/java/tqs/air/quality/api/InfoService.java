package tqs.air.quality.api;

import java.util.HashMap;

public interface InfoService {
    InfoRequest getAirQualityByLocal(double longitude, double latitude);
    HashMap<String, Integer> getCache();
}
