package tqs.air.quality.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LocationTest {

    private Location coords;

    @BeforeEach
    void setUp() {
        coords = new Location(0, 0);
    }

    @Test
    void getLongitude() {
        assertEquals(0,coords.getLongitude());
    }

    @Test
    void getLatitude() {
        assertEquals(0,coords.getLatitude());
    }

    @Test
    void testEquals() {
        assertEquals(new Location(0, 0), coords);
    }

    @Test
    void testHashCode() {
        assertEquals(new Location(0,0).hashCode(), coords.hashCode());
    }
}