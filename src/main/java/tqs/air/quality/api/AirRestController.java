package tqs.air.quality.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController

@RequestMapping("/api")
public class AirRestController {

    @Autowired
    private InfoService infoService;

    @GetMapping("/info")
    public ResponseEntity<InfoRequest> getInfoRequest(@RequestParam double lon, @RequestParam double lat) {
        return ResponseEntity.ok(infoService.getAirQualityByLocal(lon, lat));
    }

    @GetMapping("/cache")
    public ResponseEntity<HashMap<String, Integer>> getCache() {
        return new ResponseEntity<>(infoService.getCache(), HttpStatus.OK);
    }
}
