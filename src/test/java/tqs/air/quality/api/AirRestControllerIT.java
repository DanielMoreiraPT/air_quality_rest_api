package tqs.air.quality.api;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import tqs.air.quality.QualityApplication;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = QualityApplication.class)
@AutoConfigureMockMvc
public class AirRestControllerIT {
    @Autowired
    private InfoServiceImp infoService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void givenCorrectCoordsOnce_ReturnsStatus200() throws Exception {
        double lat = 48.857456;
        double lon = 2.354611;

        mockMvc.perform(get("/api/info")
                .param("lat", ""+lat)
                .param("lon", ""+lon)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(2)))
                .andExpect(jsonPath("$.fullDetails", hasKey("general_population")))
                .andExpect(jsonPath("$.fullDetails", hasKey("elderly")))
                .andExpect(jsonPath("$.fullDetails", hasKey("lung_diseases")))
                .andExpect(jsonPath("$.fullDetails", hasKey("heart_diseases")))
                .andExpect(jsonPath("$.fullDetails", hasKey("active")))
                .andExpect(jsonPath("$.fullDetails", hasKey("pregnant_women")))
                .andExpect(jsonPath("$.fullDetails", hasKey("children")))
        ;
    }
    
    @Test
    public void whenSearching_GetCacheChanges() throws Exception {
        double lat = 48.857456;
        double lon = 2.354611;

        mockMvc.perform(get("/api/info")
                .param("lat", ""+lat)
                .param("lon", ""+lon));

        mockMvc.perform(get("/api/cache")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(3)))
                .andExpect(jsonPath("$.Requests", is(2)))
                .andExpect(jsonPath("$.Miss", is(1)))
                .andExpect(jsonPath("$.Hits", is(1)))
        ;
    }
}