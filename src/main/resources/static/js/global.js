$(document).ready(function(){


    $("#GETinfo").click(function(){
        var url = "http://localhost:8080/api/info?";

        url += "lon="+$("#lon").val();
        url += "&lat="+$("#lat").val();

        console.log(url);
        $.ajax({
            type: "GET",
            url: url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                console.log(data)
                $("#active").text(data.fullDetails.active.valueOf());
                $("#children").text(data.fullDetails.children.valueOf());
                $("#elderly").text(data.fullDetails.elderly.valueOf());
                $("#general_population").text(data.fullDetails.general_population.valueOf());
                $("#heart_diseases").text(data.fullDetails.heart_diseases.valueOf());
                $("#lung_diseases").text(data.fullDetails.lung_diseases.valueOf());
                $("#pregnant_women").text(data.fullDetails.pregnant_women.valueOf());

            }
        });
    });

    $("#GETcache").click(function(){
        var url = "http://localhost:8080/api/cache";
        $.ajax({
            type: "GET",
            url: url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                console.log(data)
                $("#Requests").text(data.Requests.valueOf());
                $("#Hits").text(data.Hits.valueOf());
                $("#Miss").text(data.Miss.valueOf());
            }
        });
    });

});