package tqs.air.quality.api;

import org.springframework.stereotype.Repository;

import java.util.HashMap;

@Repository
public class DataRepository {
    /* Variables:
     *   - ttl, time-to-live
     *   - miss, # of requests that could not be fulfilled by cache
     *   - hit,  # of requests that were fulfilled by cache
     *   - cache, Hashmap to correlate location to InfoRequests
     * */
    private final int ttl;
    private int miss;
    private int hit;
    protected HashMap<Location, InfoRequest> cache;

    public DataRepository(int ttl) {
        this.ttl = ttl;
        this.miss = 0;
        this.hit = 0;
        this.cache = new HashMap<>();
    }

    public DataRepository() {
        this(600000);
    }

    // Getters
    public InfoRequest getData(double latitude, double longitude){
        Location coords = new Location(latitude, longitude);

        if (cache.containsKey(coords) && System.currentTimeMillis() < cache.get(coords).getRequestDate() + this.ttl){
            this.hit++;
            return cache.get(coords);
        }
        return null;
    }

    public int getMiss() {
        return miss;
    }

    public int getHit() {
        return hit;
    }


    public InfoRequest putData(double latitude, double longitude, String data) {
        this.miss++;
        InfoRequest infoRequest = new InfoRequest(data);
        this.cache.put(new Location(latitude,longitude), infoRequest);
        return infoRequest;
    }
}
