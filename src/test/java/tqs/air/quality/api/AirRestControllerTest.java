package tqs.air.quality.api;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest
class AirRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private InfoService infoService;
    private static InfoRequest InfoExpected;

    @BeforeAll
    static void setUp() {
        String input = "{\"metadata\": null, \"data\": {\"datetime\": \"2020-04-15T15:00:00Z\", \"data_available\": true, \"indexes\": {\"baqi\": {\"display_name\": \"BreezoMeter AQI\", \"aqi\": 56, \"aqi_display\": \"56\", \"color\": \"#DAF00F\", \"category\": \"Moderate air quality\", \"dominant_pollutant\": \"o3\"}, \"fra_atmo\": {\"display_name\": \"AQI (FR)\", \"aqi\": 5, \"aqi_display\": \"5\", \"color\": \"#FFA500\", \"category\": \"Average air quality\", \"dominant_pollutant\": \"o3\"}}, \"pollutants\": {\"co\": {\"display_name\": \"CO\", \"full_name\": \"Carbon monoxide\", \"aqi_information\": {\"baqi\": {\"display_name\": \"BreezoMeter AQI\", \"aqi\": 99, \"aqi_display\": \"99\", \"color\": \"#009E3A\", \"category\": \"Excellent air quality\"}}, \"concentration\": {\"value\": 123.93, \"units\": \"ppb\"}, \"sources_and_effects\": {\"sources\": \"Typically originates from incomplete combustion of carbon fuels, such as that which occurs in car engines and power plants.\", \"effects\": \"When inhaled, carbon monoxide can prevent the blood from carrying oxygen. Exposure may cause dizziness, nausea and headaches. Exposure to extreme concentrations can lead to loss of consciousness.\"}}, \"no2\": {\"display_name\": \"NO2\", \"full_name\": \"Nitrogen dioxide\", \"aqi_information\": {\"baqi\": {\"display_name\": \"BreezoMeter AQI\", \"aqi\": 90, \"aqi_display\": \"90\", \"color\": \"#009E3A\", \"category\": \"Excellent air quality\"}}, \"concentration\": {\"value\": 13.01, \"units\": \"ppb\"}, \"sources_and_effects\": {\"sources\": \"Main sources are fuel burning processes, such as those used in industry and transportation.\", \"effects\": \"Exposure may cause increased bronchial reactivity in patients with asthma, lung function decline in patients with COPD, and increased risk of respiratory infections, especially in young children.\"}}, \"o3\": {\"display_name\": \"O3\", \"full_name\": \"Ozone\", \"aqi_information\": {\"baqi\": {\"display_name\": \"BreezoMeter AQI\", \"aqi\": 56, \"aqi_display\": \"56\", \"color\": \"#FFFF00\", \"category\": \"Moderate air quality\"}}, \"concentration\": {\"value\": 55.5, \"units\": \"ppb\"}, \"sources_and_effects\": {\"sources\": \"Ozone is created in a chemical reaction between atmospheric oxygen, nitrogen oxides, carbon monoxide and organic compounds, in the presence of sunlight.\", \"effects\": \"Ozone can irritate the airways and cause coughing, a burning sensation, wheezing and shortness of breath. Additionally, ozone is one of the major components of photochemical smog.\"}}, \"pm10\": {\"display_name\": \"PM10\", \"full_name\": \"Inhalable particulate matter (<10\\u00b5m)\", \"aqi_information\": {\"baqi\": {\"display_name\": \"BreezoMeter AQI\", \"aqi\": 68, \"aqi_display\": \"68\", \"color\": \"#84CF33\", \"category\": \"Good air quality\"}}, \"concentration\": {\"value\": 35.62, \"units\": \"ug/m3\"}, \"sources_and_effects\": {\"sources\": \"Main sources are combustion processes (e.g. indoor heating, wildfires), mechanical processes (e.g. construction, mineral dust, agriculture) and biological particles (e.g. pollen, bacteria, mold).\", \"effects\": \"Inhalable particles can penetrate into the lungs. Short term exposure can cause irritation of the airways, coughing, and aggravation of heart and lung diseases, expressed as difficulty breathing, heart attacks and even premature death.\"}}, \"pm25\": {\"display_name\": \"PM2.5\", \"full_name\": \"Fine particulate matter (<2.5\\u00b5m)\", \"aqi_information\": {\"baqi\": {\"display_name\": \"BreezoMeter AQI\", \"aqi\": 71, \"aqi_display\": \"71\", \"color\": \"#84CF33\", \"category\": \"Good air quality\"}}, \"concentration\": {\"value\": 18.23, \"units\": \"ug/m3\"}, \"sources_and_effects\": {\"sources\": \"Main sources are combustion processes (e.g. power plants, indoor heating, car exhausts, wildfires), mechanical processes (e.g. construction, mineral dust) and biological particles (e.g. bacteria, viruses).\", \"effects\": \"Fine particles can penetrate into the lungs and bloodstream. Short term exposure can cause irritation of the airways, coughing and aggravation of heart and lung diseases, expressed as difficulty breathing, heart attacks and even premature death.\"}}, \"so2\": {\"display_name\": \"SO2\", \"full_name\": \"Sulfur dioxide\", \"aqi_information\": {\"baqi\": {\"display_name\": \"BreezoMeter AQI\", \"aqi\": 100, \"aqi_display\": \"100\", \"color\": \"#009E3A\", \"category\": \"Excellent air quality\"}}, \"concentration\": {\"value\": 0.23, \"units\": \"ppb\"}, \"sources_and_effects\": {\"sources\": \"Main sources are burning processes of sulfur-containing fuel in industry, transportation and power plants.\", \"effects\": \"Exposure causes irritation of the respiratory tract, coughing and generates local inflammatory reactions. These in turn, may cause aggravation of lung diseases, even with short term exposure.\"}}}, \"health_recommendations\": {\"general_population\": \"If you start to feel respiratory discomfort such as coughing or breathing difficulties, consider reducing the intensity of your outdoor activities.\", \"elderly\": \"Reduce the intensity of your outdoor activities or postpone them to the early morning when ozone levels tend to be lower. In addition, consider reducing the time you spend near busy roads, construction sites, open fires and other sources of smoke.\", \"lung_diseases\": \"Reduce the intensity of your outdoor activities or postpone them to the early morning when ozone levels tend to be lower. Keep relevant medication(s) available and consult a doctor with any questions. In addition, consider reducing the time you spend near busy roads, industrial emission stacks, open fires and other sources of smoke.\", \"heart_diseases\": \"If you start to feel respiratory discomfort such as coughing or breathing difficulties, consider reducing the intensity of your outdoor activities. Try to limit the time you spend near busy roads, construction sites, industrial emission stacks, open fires and other sources of smoke.\", \"active\": \"Reduce the intensity of your outdoor activities or postpone them to the early morning when ozone levels tend to be lower. In addition, consider reducing the time you spend near busy roads, construction sites, industrial emission stacks, open fires and other sources of smoke.\", \"pregnant_women\": \"To keep you and your baby healthy, reduce the intensity of your outdoor activities or postpone them to the early morning when ozone levels tend to be lower. In addition, consider reducing the time you spend near busy roads, construction sites, open fires and other sources of smoke.\", \"children\": \"Reduce the intensity of your outdoor activities or postpone them to the early morning when ozone levels tend to be lower. In addition, consider reducing the time you spend near busy roads, construction sites, open fires and other sources of smoke.\"}}, \"error\": null}";
        InfoExpected = new InfoRequest(input);
    }

    @BeforeEach
    public void setupMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getInfoRequest() throws Exception{
        when(infoService.getAirQualityByLocal(anyDouble(), anyDouble()))
            .thenReturn(InfoExpected);

        mockMvc.perform(get("/api/info")
                .param("lon", "0.0")
                .param("lat", "0.0")
                .contentType(MediaType.APPLICATION_JSON)).
                andExpect(status().isOk());

        verify(infoService, times(1)).getAirQualityByLocal(anyDouble(), anyDouble());
        reset(infoService);
    }

    @Test
    void getCache() throws Exception {
        HashMap<String, Integer> cache = new HashMap<>();

        cache.put("Hits", 3);
        cache.put("Miss", 1);
        cache.put("Requests", 4);

        when(infoService.getCache())
                .thenReturn(cache);

        mockMvc.perform(get("/api/cache")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(3)))
                .andExpect(jsonPath("$.Requests", is(4)))
                .andExpect(jsonPath("$.Miss", is(1)))
                .andExpect(jsonPath("$.Hits", is(3)))
        ;
    }
}