package tqs.air.quality.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class InfoRequest {
    private final long requestDate;
    private int baqi;

    private final ObjectMapper objectMapper;
    private final Map<String, String> details;

    public InfoRequest(String json) {
        this.requestDate = System.currentTimeMillis();
        this.objectMapper = new ObjectMapper();
        this.details = new HashMap<>();
        this.parseJson(json);
    }

    private void parseJson(String json) {
        JsonNode jsonNode = null;

        try {
            jsonNode = objectMapper.readTree(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        assert jsonNode != null;
        this.baqi = jsonNode.get("data").get("indexes").get("baqi").get("aqi").asInt();

        JsonNode healthRecommendations = jsonNode.get("data").get("health_recommendations");

        // Data Insert
        {
            details.put("general_population", healthRecommendations.get("general_population").asText());
            details.put("elderly", healthRecommendations.get("elderly").asText());
            details.put("lung_diseases", healthRecommendations.get("lung_diseases").asText());
            details.put("heart_diseases", healthRecommendations.get("heart_diseases").asText());
            details.put("active", healthRecommendations.get("active").asText());
            details.put("pregnant_women", healthRecommendations.get("pregnant_women").asText());
            details.put("children", healthRecommendations.get("children").asText());
        }
    }

    @Override
    public String toString() {
        return "InfoRequest{" +
                "requestDate=" + requestDate +
                ", baqi=" + baqi +
                ", objectMapper=" + objectMapper +
                ", details=" + details +
                '}';
    }

    // Getters
    public long getRequestDate() {
        return requestDate;
    }

    public String getDetails(String string) {

        return details.get(string);
    }

    public Map<String, String> getFullDetails() {

        return this.details;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InfoRequest that = (InfoRequest) o;
        return requestDate == that.requestDate &&
                baqi == that.baqi &&
                Objects.equals(objectMapper, that.objectMapper) &&
                Objects.equals(details, that.details);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestDate, baqi, objectMapper, details);
    }
}
