package tqs.air.quality.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;

@Service
public class InfoServiceImp implements InfoService {

    @Value("${breezometer.api}")
    private String url;

    @Value("${breezometer.key}")
    private String key;

    @Value("${breezometer.features}")
    private String features;

    @Autowired
    private DataRepository dataRepository;

    private final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .build();

    protected String sendGET(String lat, String lon) throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(
                        this.url+
                                "lat=" + lat +
                                "&lon=" + lon +
                                "&key=" + this.key +
                                "&features=" + this.features))
                .setHeader("User-Agent", "Java 11 HttpClient Bot")
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        return response.body();
    }

    @Override
    public InfoRequest getAirQualityByLocal(double longitude, double latitude) {
        InfoRequest infoRequest = dataRepository.getData(longitude, latitude);
        if (infoRequest == null){
            try {
                String getResult = sendGET("" + latitude , "" + longitude);

                System.out.println(getResult);

                infoRequest = new InfoRequest(getResult);

                this.dataRepository.putData(longitude, latitude, getResult);
            } catch (Exception e){
                System.out.println(e);
                return null;
            }
        }

        return infoRequest;
    }

    @Override
    public HashMap<String, Integer> getCache() {
        HashMap<String, Integer> cache = new HashMap<>();

        cache.put("Requests", dataRepository.getHit() + dataRepository.getMiss());
        cache.put("Hits", dataRepository.getHit());
        cache.put("Miss", dataRepository.getMiss());

        return cache;
    }
}
